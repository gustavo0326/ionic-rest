angular.module('app.services', [])

.factory('BlankFactory', [function(){

}])

.service('serializer', [function(){
    this.prepareData = function (obj){
      var r = "";
      for (field in obj){
        r+= escape(field)+"="+escape(obj[field])+"&";
      }
      return r.slice(0,-1);
    }
}]);
